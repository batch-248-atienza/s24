console.log("Hello World");

let num = 2;
let getCube = num**3;

console.log(`The cube of ${num} is ${getCube}`);

let address = ["258 Washington Ave NW", "California", 90011];
let [street, state, zipCode] = address;
console.log(`I live at ${street}, ${state} ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
};

let {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighed ${weight} with a measurement of ${length}.`);

let arrayOfNumbers = [1,2,3,4,5];

arrayOfNumbers.forEach((number)=>{
	console.log(number);
});

let reduceNumber = arrayOfNumbers.reduce((num1,num2)=>num1 + num2);
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

let newDog = new Dog("Frankie", 5, "Miniature Daschund");
console.log(newDog);